INSERT INTO role (id, role_name, description, created_at, updated_at) VALUES (1, 'STANDARD_USER', 'Standard User - Has no admin rights', now(), now());
INSERT INTO role (id, role_name, description, created_at, updated_at) VALUES (2, 'ADMIN_USER', 'Admin User - Has permission to perform admin tasks', now(), now());


INSERT INTO public.users (id, full_name, password, username, email, created_at, updated_at) VALUES (1, 'Meilina Putri', '$2y$12$IK7itFEnuhx9Gf6w0gS/z.OTy3H84dmHqfHaVA485qzy1I1QQujdy', 'meilina.putri','meilina@gmail.com', now(), now());
INSERT INTO public.users (id, full_name, password, username,  email, created_at, updated_at) VALUES (2, 'Abi Tulkhah', '$2y$12$IK7itFEnuhx9Gf6w0gS/z.OTy3H84dmHqfHaVA485qzy1I1QQujdy', 'abi.tulkhah','abi.tulkhah@gmail.com', now(), now());

INSERT INTO user_role(id, user_id, role_id, created_at, updated_at) VALUES(1, 1, 1, now(), now());
INSERT INTO user_role(id, user_id, role_id, created_at, updated_at) VALUES(2, 2, 1, now(), now());
INSERT INTO user_role(id, user_id, role_id, created_at, updated_at) VALUES(3, 2, 2, now(), now());


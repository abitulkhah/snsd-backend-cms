package com.snsd.putri.SNSDPutri.dao;

import com.snsd.putri.SNSDPutri.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends CrudRepository<Users,Long> {
    Users findByUsername(String username);
}

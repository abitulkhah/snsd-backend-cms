package com.snsd.putri.SNSDPutri.controller;


import com.snsd.putri.SNSDPutri.dto.SongDto;
import com.snsd.putri.SNSDPutri.model.Song;
import com.snsd.putri.SNSDPutri.repositories.SongRepository;
import org.json.simple.JSONObject;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import utils.EncoderImage;

import java.io.IOException;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/v1/song")
public class SongController {

    @Autowired
    SongRepository songRepository;

    private EncoderImage encoderImage = new EncoderImage();

    private ModelMapper modelMapper = new ModelMapper();

    @PostMapping
    public ResponseEntity<JSONObject> createSong (@Validated @RequestBody SongDto songDto) throws IOException {
        JSONObject jsonObject = new JSONObject();

        encoderImage.base64Decode(songDto.getBase64Data(), songDto.getImage());
        Song song = modelMapper.map(songDto, Song.class);
        songRepository.save(song);

        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success Create Song");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<JSONObject> getSong (){
        JSONObject jsonObject = new JSONObject();
        List<Song> listSong = songRepository.findAll();
        try{

            for (Song song: listSong){
                song.setImage(encoderImage.base64Encode(song.getImage()));
            }

            jsonObject.put("status", 200);
            jsonObject.put("message", "Success get All Song");
            jsonObject.put("data", listSong);
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<JSONObject> updateSong (@Validated @RequestBody SongDto songDto){
        JSONObject jsonObject = new JSONObject();
        try{
            Song song = songRepository.findById(songDto.getId()).get();
            song.setTitle(songDto.getTitle() !=null ? songDto.getTitle():song.getTitle());
            song.setWriter(songDto.getWriter() !=null ? songDto.getWriter():song.getWriter());
            song.setAlbum(songDto.getAlbum() !=null ? songDto.getAlbum():song.getAlbum());
            song.setYear(songDto.getYear() !=null ? songDto.getYear():song.getYear());
            song.setYoutube(songDto.getYoutube() !=null ? songDto.getYoutube():song.getYoutube());
            song.setUpdatedAt(new Date());
            songRepository.save(song);
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success update Song");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }


    @DeleteMapping
    public ResponseEntity<JSONObject> deleteSong (@Validated @RequestBody SongDto songDto){
        JSONObject jsonObject = new JSONObject();
        songRepository.deleteById(songDto.getId());
        try{
            jsonObject.put("status", 200);
            jsonObject.put("message", "Success delete Song");
        }catch (Exception e){
            jsonObject.put("message", e.toString());
        }

        return new ResponseEntity<JSONObject>(jsonObject, HttpStatus.OK);
    }

}

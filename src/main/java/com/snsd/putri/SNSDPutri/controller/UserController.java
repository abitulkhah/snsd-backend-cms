package com.snsd.putri.SNSDPutri.controller;

import com.snsd.putri.SNSDPutri.model.Users;
import com.snsd.putri.SNSDPutri.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

//    @RequestMapping(value="/user", method = RequestMethod.GET)
    @GetMapping
    public List<com.snsd.putri.SNSDPutri.model.Users> listUser(){
        return userService.findAll();
    }

//    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @PostMapping
    public Users create(@RequestBody com.snsd.putri.SNSDPutri.model.Users users){
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        return userService.save(users);
    }

//    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    @DeleteMapping
    public String delete(@PathVariable(value = "id") Long id){
        userService.delete(id);
        return "success";
    }

}

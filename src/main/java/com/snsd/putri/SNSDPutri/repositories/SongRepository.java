package com.snsd.putri.SNSDPutri.repositories;

import com.snsd.putri.SNSDPutri.model.Song;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongRepository extends JpaRepository<Song, Long> {

}

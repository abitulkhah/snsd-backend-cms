package com.snsd.putri.SNSDPutri.repositories;

import com.snsd.putri.SNSDPutri.model.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
    public List<Answer> findByQuestionId(Long questionId);
}

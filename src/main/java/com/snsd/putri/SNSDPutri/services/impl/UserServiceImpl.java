package com.snsd.putri.SNSDPutri.services.impl;

import com.snsd.putri.SNSDPutri.dao.UserDao;
import com.snsd.putri.SNSDPutri.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {

    @Autowired
    private UserDao userDao;

    public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
        com.snsd.putri.SNSDPutri.model.Users users = userDao.findByUsername(userId);
        if(users == null){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(users.getUsername(), users.getPassword(), getAuthority());
    }

    private List<SimpleGrantedAuthority> getAuthority() {
        return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
    }


    @Override
    public com.snsd.putri.SNSDPutri.model.Users save(com.snsd.putri.SNSDPutri.model.Users user) {
        return userDao.save(user);
    }

    public List<com.snsd.putri.SNSDPutri.model.Users> findAll() {
        List<com.snsd.putri.SNSDPutri.model.Users> list = new ArrayList<>();
        userDao.findAll().iterator().forEachRemaining(list::add);
        return list;
    }

    @Override
    public void delete(long id) {
        userDao.deleteById(id);
    }


}
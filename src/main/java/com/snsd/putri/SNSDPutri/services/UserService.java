package com.snsd.putri.SNSDPutri.services;


import com.snsd.putri.SNSDPutri.model.Users;

import java.util.List;

public interface UserService {
    Users save(Users user);
    List<Users> findAll();
    void delete(long id);
}

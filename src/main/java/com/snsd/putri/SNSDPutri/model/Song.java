package com.snsd.putri.SNSDPutri.model;

import javax.persistence.*;

@Entity
@Table(name = "song")
public class Song extends AuditModel {


    @Id
    @GeneratedValue(generator = "song_generator")
    @SequenceGenerator(
            name = "song_generator",
            sequenceName = "song_sequence",
            initialValue = 1
    )
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "album")
    private String album;

    @Column(name = "writer")
    private String writer;

    @Column(name = "year")
    private String year;

    @Column(name = "image")
    private String image;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "youtube")
    private String youtube;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }
}


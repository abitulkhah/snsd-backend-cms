package com.snsd.putri.SNSDPutri.model;

import javax.persistence.*;


@Entity
@Table(name = "user_role")
public class UserRole extends AuditModel {
    @Id
    @GeneratedValue(generator = "user_role_generator")
    @SequenceGenerator(
            name = "user_role_generator",
            sequenceName = "user_role_sequence",
            initialValue = 1
    )
    private Long id;

    @Column(name="userId")
    private Long userId;

    @Column(name="roleId")
    private Long roleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}

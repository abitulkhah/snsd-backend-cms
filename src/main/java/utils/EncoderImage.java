package utils;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.io.IOException;
import java.util.Base64;

public class EncoderImage {

    @Value("${image.path}")
    private String imagePath;


    public String base64Encode(String imageName) throws IOException {

        byte[] fileContent = FileUtils.readFileToByteArray(new File(imagePath+"/"+imageName));
        String encodedString = Base64.getEncoder().encodeToString(fileContent);

        return encodedString;

    }

    public void base64Decode(String encodedString, String fileName) throws IOException {

        byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
        FileUtils.writeByteArrayToFile(new File(imagePath+"/"+fileName), decodedBytes);

    }

}
